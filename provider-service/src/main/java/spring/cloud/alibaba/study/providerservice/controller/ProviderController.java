package spring.cloud.alibaba.study.providerservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import spring.cloud.alibaba.study.providerservice.model.vo.Result;
import spring.cloud.alibaba.study.providerservice.service.ProviderService;
import spring.cloud.alibaba.study.providerservice.util.ResultUtil;

@RestController
public class ProviderController {

    @Autowired
    private ProviderService providerService;

    @GetMapping(value = "/port",produces="application/json;charset=UTF-8")
    public Result acquire() {
        String acquire = providerService.acquirePort();
        if (StringUtils.isEmpty(acquire)) {
            return ResultUtil.error(-1, "wu");
        }
        return ResultUtil.success(acquire);
    }

}
