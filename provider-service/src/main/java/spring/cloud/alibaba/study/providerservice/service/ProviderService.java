package spring.cloud.alibaba.study.providerservice.service;

public interface ProviderService {

    String acquirePort();

}
