package spring.cloud.alibaba.study.providerservice.util;

import spring.cloud.alibaba.study.providerservice.model.vo.Result;

public class ResultUtil {
    public static Result success(Object data){
        Result objectResult = new Result<>();

        objectResult.setSuccess(true);
        objectResult.setCode(0);
        objectResult.setData(data);

        return objectResult;
    }

    public static Result error(Integer code, String massage){
        Result objectResult = new Result<>();
        objectResult.setSuccess(false);
        objectResult.setCode(code);
        objectResult.setMessage(massage);
        return objectResult;
    }

}
