package spring.cloud.alibaba.study.providerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringCloudApplication
public class ProviderServiceApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(ProviderServiceApplication.class, args);
        String property = configurableApplicationContext.getEnvironment().getProperty("user.test");
        System.out.println(property);
    }
}
