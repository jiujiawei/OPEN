package spring.cloud.alibaba.study.providerservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import spring.cloud.alibaba.study.providerservice.config.TomcatConfig;
import spring.cloud.alibaba.study.providerservice.service.ProviderService;

@Service
public class ProviderServiceImpl implements ProviderService {
    @Autowired
    private TomcatConfig tomcatConfig ;

    @Override
    public String acquirePort() {
//        applicationContext.get
        return String.valueOf(tomcatConfig.getProt());
    }
}
